﻿$(document).ready(function () {
    $(".moreBox").slice(0, 3).show();
    if ($(".blogBox:hidden").length != 0) {
        $("#loadMore").show();
    }
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        var type = readCookie("type");
        var loadImages = readCookie("loadImages");
        var imageCounter = readCookie("imageCounter");
        imageCounter = parseInt(imageCounter) + 1;
        var maxImageCount = readCookie("maxImageCount");
        var blockSize = readCookie("blockSize");
        if (parseInt(imageCounter) >= parseInt(maxImageCount)) {
            $("#loadMore").hide();
            return;
        }
        $("#div1").load("/loadmore/" + type + "/" + loadImages + "/" + blockSize + "/" + maxImageCount + "/" + imageCounter);
    });
    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }
});