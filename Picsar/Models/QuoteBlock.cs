﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Picsar.Models
{
    public class QuoteBlock
    {
        public IList<string> ImageUrl
        {
            get; set;
        }
        public bool IsFirstBlock
        {
            get;set;
        }
    }
}
