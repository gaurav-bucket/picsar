﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Picsar.Models
{ 
    public class Quotes
    {
        public IList<QuoteBlock> QuoteBlocks
        {
            get;set;
        }
        public string Headline
        {
            get;set;
        }
        public bool IsMobile
        {
            get;set;
        }
    }
}
