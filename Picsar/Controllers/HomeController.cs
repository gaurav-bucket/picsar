﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Picsar.Models;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using System.Web;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
namespace Picsar.Controllers
{
    public class HomeController : Controller
    {
        IConfiguration _iConfiguration;
        public HomeController(IConfiguration iConfiguration) 
        {
            _iConfiguration = iConfiguration;
        }
        public IActionResult test()
        {
            return View("popular");
        }
        public IActionResult Index()
        {
            SetUniqueUserID();
            return View();
        }
        private void SetUniqueUserID()
        {
            if (Request.Cookies["userID"] != null)
            {
                return;
            }
            string userID = string.Empty;
            Guid g;
            g = Guid.NewGuid();
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Append("userID", g.ToString(), option);

        }

        private void SetType(string type)
        {
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Append("type", type, option);
        }
        private void SetLoadImages(string loadImages)
        {
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Append("loadImages", loadImages, option);
        }
        private void SetBlockSize(string blockSize)
        {
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Append("blockSize", blockSize, option);
        }
        private void SetMaxImageCount(string maxImageCount)
        {
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Append("maxImageCount", maxImageCount, option);
        }
        private void SetImageCounter(string imageCounter)
        {
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Append("imageCounter", imageCounter, option);
        }
        private void DeleteImageCounter()
        {
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Delete("imageCounter");
        }
        private bool IsMobile()
        {
            string userAgent = Request.Headers["User-Agent"];
            Regex OS = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex device = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            string device_info = string.Empty;
            if (OS.IsMatch(userAgent))
            {
                device_info = OS.Match(userAgent).Groups[0].Value;
            }
            if (device.IsMatch(userAgent.Substring(0, 4)))
            {
                device_info += device.Match(userAgent).Groups[0].Value;
            }
            if (!string.IsNullOrEmpty(device_info))
            {
                return true;
            }
            return false;
        }
        [Route("/quotes/{type}")]
        public IActionResult Quotes(string type)
        {
            DeleteImageCounter();
            int loadImages = Convert.ToInt32(_iConfiguration["LoadImages"]);
            int blockSize = Convert.ToInt32(_iConfiguration["BlockSize"]);
            int maxImageCount = Convert.ToInt32(_iConfiguration["MaxImageCount"]);

            SetUniqueUserID();
            SetType(type);
            SetBlockSize(blockSize.ToString());
            SetMaxImageCount(maxImageCount.ToString());
            SetLoadImages(loadImages.ToString());
            Quotes quotes = GetQuotes(type, loadImages, blockSize, maxImageCount);

            if (type == "popular-on-whatsapp-facebook")
            {
                quotes.Headline = "popular on whatsapp/facebook 👍";
            }
            else if (type == "feeling-happy-met-with-old-friends")
            {
                quotes.Headline = "feeling happy as I met with old friends 😊";
            }
            else if (type == "wanna-show-attitude-to-friends")
            {
                quotes.Headline = "wanna show attitude to friends 😏";
            }
            else if (type == "nobody-is-understanding-me")
            {
                quotes.Headline = "nobody is understanding me 😒";
            }
            else if (type == "missing-gf-bf-spouse")
            {
                quotes.Headline = "missing my spouse/gf/bg 😔";
            }
            else if (type == "some-health-quotes-please")
            {
                quotes.Headline = "some health quotes please 💪";
            }
            quotes.IsMobile = IsMobile();
            return View("Quotes", quotes);
        }
        [Route("/loadmore/{type}/{loadImages}/{blockSize}/{maxImageCount}/{imageCounter}")]
        public PartialViewResult LoadMore(string type, string loadImages, string blockSize, string maxImageCount, string imageCounter)
        {
            Quotes quotes = GetQuotes(type, Convert.ToInt32(loadImages), Convert.ToInt32(blockSize), Convert.ToInt32(maxImageCount), Convert.ToInt32(imageCounter));

            return new PartialViewResult
            {
                ViewName = "LoadMore",
                ViewData = new ViewDataDictionary<Quotes>(ViewData, quotes)
            };
        }
        private Quotes GetQuotes(string type, int loadImages, int blockSize, int maxImageCount, int imageCounter = 1)
        {

            Quotes quotes = new Quotes();
            IList<QuoteBlock> quoteBlocks = new List<QuoteBlock>();
            QuoteBlock _quoteBlock = null;
            string baseUrl = "https://imagequotes.blob.core.windows.net/imagequotes/" + type + "/";
            IList<string> imageUrl = new List<string>();
            string url = string.Empty;
            int numOfBlocks = (loadImages / blockSize);
            //int imageCounter = 1;
            for (int blockCount = 1; blockCount <= numOfBlocks; blockCount++)
            {
                _quoteBlock = new QuoteBlock();
                imageUrl = new List<string>();
                if (blockCount == 1)
                {
                    _quoteBlock.IsFirstBlock = true;
                }
                for (int counter = 0; counter < blockSize; counter++)
                {
                    url = baseUrl + imageCounter.ToString() + ".png";
                    imageUrl.Add(url);
                    SetImageCounter(imageCounter.ToString());
                    imageCounter = imageCounter + 1;
                    if (imageCounter > maxImageCount)
                    {
                        _quoteBlock.ImageUrl = imageUrl;
                        quoteBlocks.Add(_quoteBlock);
                        quotes.QuoteBlocks = quoteBlocks;
                        return quotes;
                    }
                }
                _quoteBlock.ImageUrl = imageUrl;
                quoteBlocks.Add(_quoteBlock);
            }
            quotes.QuoteBlocks = quoteBlocks;
            return quotes;
        }


        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
